package das.iker.labo_3.ui

import android.app.Activity
import android.content.Intent
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.rounded.Check
import androidx.compose.material.icons.rounded.CheckCircle
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import das.iker.labo_3.AddTodoItemActivity
import das.iker.labo_3.R
import das.iker.labo_3.TodoItem
import das.iker.labo_3.TodoListViewModel

@Composable
fun TodoListScreen(appViewModel: TodoListViewModel) {
    Scaffold(
        topBar = { AppBar() },
        floatingActionButton = { AddFAB(onAdd = appViewModel::addItem) },
        floatingActionButtonPosition = FabPosition.Center,
    ) {
        TodoList(appViewModel)
    }
}


@Composable
private fun AddFAB(onAdd: (TodoItem) -> Unit) {
    val context = LocalContext.current
    val resultLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult())
        { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                // There are no request codes
                val data: Intent? = result.data
                val newTodoItem = data?.getParcelableExtra<TodoItem>("newTodoItem")
                if (newTodoItem != null) onAdd(newTodoItem)
            }
        }

    FloatingActionButton(
        modifier = Modifier.fillMaxWidth(0.9f),
        onClick = {
            val intent = Intent(context, AddTodoItemActivity::class.java)
            resultLauncher.launch(intent)
        }
    ) {
        Row(
            modifier = Modifier.padding(horizontal = 16.dp)
        ) {
            Icon(
                imageVector = Icons.Default.Add,
                contentDescription = null
            )
            // Toggle the visibility of the content with animation.
            Text(
                text = "Añadir Ítem",
                modifier = Modifier
                    .padding(start = 8.dp, top = 3.dp)
            )

        }
    }
}

@Composable
private fun AppBar() {
    TopAppBar(
        navigationIcon = {
            Icon(
                imageVector = Icons.Rounded.CheckCircle,
                contentDescription = null,
                modifier = Modifier.padding(horizontal = 12.dp),
            )
        },
        title = {
            Text(text = stringResource(R.string.app_list_title))
        },
        backgroundColor = MaterialTheme.colors.primarySurface
    )
}

@Composable
private fun TodoList(appViewModel: TodoListViewModel) {
    val context = LocalContext.current

    LazyColumn {
        items(
            items = appViewModel.todoItems,
            key = { it.id }
        ) { todoItem ->
            TodoListItem(
                item = todoItem,
                onLongClick = {
                    appViewModel.removeItem(it)
                    Toast.makeText(
                        context,
                        "TODO Item deleted",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            )

        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun TodoListItem(
    item: TodoItem,
    onLongClick: (TodoItem) -> Unit,
) {
    ListItem(
        modifier = Modifier.pointerInput(Unit) {
            detectTapGestures(
                onLongPress = {
                    onLongClick(item)
                }
            )
        },
        icon = {
            Icon(
                Icons.Rounded.Check, null,
                tint = MaterialTheme.colors.primary
            )
        }
    ) {
        Text(item.task)
    }
    Divider()
}
