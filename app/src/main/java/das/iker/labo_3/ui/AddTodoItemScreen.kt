package das.iker.labo_3.ui

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Intent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AddCircle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import das.iker.labo_3.R
import das.iker.labo_3.TodoItem

@Composable
fun AddTodoItemScreen() {
    Scaffold(
        topBar = { AppBar() }
    ) {
        AddItemMainContent()
    }
}

//--------------------------------------------------------------

@Composable
private fun AppBar() {
    TopAppBar(
        navigationIcon = {
            Icon(
                imageVector = Icons.Rounded.AddCircle,
                contentDescription = null,
                modifier = Modifier.padding(horizontal = 12.dp),
            )
        },
        title = {
            Text(text = stringResource(R.string.app_add_item_title))
        },
        backgroundColor = MaterialTheme.colors.primarySurface
    )
}


@Composable
fun AddItemMainContent() {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        contentAlignment = Alignment.Center
    ) {
        Card {
            Column(
                Modifier.padding(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                val activity = (LocalContext.current as? Activity)
                val (text, setText) = remember { mutableStateOf("") }
                TextField(value = text, onValueChange = setText, singleLine = true)
                Button(onClick = {
                    val intent = Intent()
                    intent.putExtra("newTodoItem", TodoItem(task = text))
                    activity?.setResult(RESULT_OK, intent)
                    activity?.finish()
                }) {
                    Text(text = "Add Item")
                }
            }
        }
    }
}