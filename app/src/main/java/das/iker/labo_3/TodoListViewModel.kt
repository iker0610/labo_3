package das.iker.labo_3


import android.os.Parcelable
import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class TodoItem(
    val id: UUID = UUID.randomUUID(),
    val task: String,
) : Parcelable

class TodoListViewModel : ViewModel() {
    val todoItems = mutableStateListOf<TodoItem>()

    fun addItem(item: TodoItem) {
        todoItems.add(item)
    }

    fun removeItem(item: TodoItem) {
        todoItems.remove(item)
    }
}